#!/bin/bash

echo "Hello!"

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

# Log into GitLab's container repository.
export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

buildah bud -t testimage .

CONTAINER_ID=$(buildah from testimage)
buildah commit --squash $CONTAINER_ID registry.dune-project.org/smuething/imagetest/testimage:latest

buildah push registry.dune-project.org/smuething/imagetest/testimage:latest
